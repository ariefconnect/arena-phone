package com.lupawktu.arenaphone.Result;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mind on 6/19/2017.
 */

public class ResultAllBrand {
    private String slug = "slug";

    /**
     * slug
     * @return
     */
    public String getSlug() {
        return slug;
    }

    /**
     * set slug
     * @param slug
     */
    public void setSlug(String slug) {
        this.slug = slug;
    }
}
