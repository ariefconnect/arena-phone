package com.lupawktu.arenaphone.presenter;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lupawktu.arenaphone.URL;
import com.lupawktu.arenaphone.model.ResponseDataList;

import cz.msebera.android.httpclient.Header;
import okhttp3.Response;

/**
 * Created by Mind on 7/10/2017.
 */

public class LoadDataDetail {
    private ResponseDataList model;
    private View view;
    public LoadDataDetail(View view) {
        this.view = view;
    }

    /**
     * init method
     * @param slug
     */
    public void getDataSpecDetail(String slug) {
        findDataInServer(URL.rootUrl, slug);
    }

    /**
     * find data on server
     * @param rootUrl
     * @param slug
     */
    private void findDataInServer(String rootUrl, String slug) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("slug", slug);
        client.get(rootUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String data = new String(responseBody);
                String first = String.valueOf(data.charAt(0));
                if(first.equalsIgnoreCase("=")){
                    data = data.replace("=","");
                }
                dataOnSuccess(statusCode, data);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                dataFailed(statusCode, "Error get data, please try again.");
            }
        });
    }

    /**
     * failure
     * @param statusCode
     * @param data
     */
    private void dataFailed(int statusCode, String data) {
        model = new ResponseDataList();
        model.setData(data);
        model.setCode(statusCode);
        view.getDataFailure(model);
    }

    /**
     * success
     * @param statusCode
     * @param data
     */
    private void dataOnSuccess(int statusCode, String data) {
        model = new ResponseDataList();
        model.setData(data);
        model.setCode(statusCode);
        view.getDataSuccess(model);
    }


    public interface View {
        void getDataSuccess(ResponseDataList model);
        void getDataFailure(ResponseDataList model);
    }
}
