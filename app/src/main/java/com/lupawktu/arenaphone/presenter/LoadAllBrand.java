package com.lupawktu.arenaphone.presenter;

import android.app.Activity;

import com.lupawktu.arenaphone.AllBrand;
import com.lupawktu.arenaphone.Result.ResultAllBrand;
import com.lupawktu.arenaphone.model.ResponseDataBrand;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Mind on 6/19/2017.
 */

public class LoadAllBrand {
    private ViewData view;
    private ResponseDataBrand model;
    public LoadAllBrand(ViewData view) {
        this.view = view;
    }

    /**
     * load json from file
     * @return json
     * @param allBrand
     */
    public String loadJSONFromAsset(AllBrand allBrand) {
        String json = null;
        try {
            InputStream is = allBrand.getAssets().open("allbrand.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void loadDataJson(AllBrand allBrand) {
        String data = loadJSONFromAsset(allBrand);
        model = new ResponseDataBrand();
        model.setData(data);
        view.getData(model);
    }

    public interface ViewData{
        void getData(ResponseDataBrand model);
    }
}
