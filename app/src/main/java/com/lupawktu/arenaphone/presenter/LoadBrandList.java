package com.lupawktu.arenaphone.presenter;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lupawktu.arenaphone.ListBrand;
import com.lupawktu.arenaphone.URL;
import com.lupawktu.arenaphone.model.ArrayDataBrand;
import com.lupawktu.arenaphone.model.DataList;
import com.lupawktu.arenaphone.model.ResponseDataList;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Mind on 7/8/2017.
 */

public class LoadBrandList {
    private ViewBrand view;
    private ResponseDataList model;
    private ArrayDataBrand arrayData;
    private DataList dl;

    public LoadBrandList(ViewBrand view){
        this.view = view;
    }

    /**
     * presenter get data
     * @param slug
     */
    public void getDataListBrand(String slug) {
        requestToURL(URL.rootUrl, slug);
    }

    /**
     * request data from url
     * @param rootUrl
     * @param slug
     */
    private void requestToURL(String rootUrl, String slug) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("query", slug);

        client.get(rootUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String data = new String(responseBody);
                String first = String.valueOf(data.charAt(0));
                if(first.equalsIgnoreCase("=")){
                    data = data.replace("=","");
                }
                dataSuccess(statusCode, data);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                dataFailed(statusCode, "Error get data, please try again.");
            }
        });
    }

    /**
     * add fail get data to model
     * @param statusCode
     * @param s
     */
    private void dataFailed(int statusCode, String s) {
        model = new ResponseDataList();
        model.setData(s);
        model.setCode(statusCode);
        view.failGetData(model);
    }

    /**
     * add success get data to model
     * @param statusCode
     * @param s
     */
    private void dataSuccess(int statusCode, String s) {
        model = new ResponseDataList();
        model.setData(s);
        model.setCode(statusCode);
        view.getData(model);
    }

    /**
     * save data to array
     * @param data
     */
    public void SavaDataToArray(String data) {
        try {
            ArrayList<DataList> brandData = new ArrayList<>();
            JSONArray array = new JSONArray(data);
            for ( int a = 0; a < array.length(); a++ ){
                JSONObject obj = array.getJSONObject(a);
                dl = new DataList();
                dl.setTitle(obj.getString("title"));
                dl.setSlug(obj.getString("slug"));
                brandData.add(dl);
            }
            arrayData = new ArrayDataBrand();
            arrayData.setDataLists(brandData);
            view.showData(arrayData);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public interface ViewBrand{
        void getData(ResponseDataList model);
        void failGetData(ResponseDataList model);
        void showData(ArrayDataBrand model);
    }
}
