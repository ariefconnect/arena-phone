package com.lupawktu.arenaphone;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import com.lupawktu.arenaphone.Result.ResultAllBrand;
import com.lupawktu.arenaphone.adapter.BrandAdapter;
import com.lupawktu.arenaphone.model.ResponseDataBrand;
import com.lupawktu.arenaphone.presenter.LoadAllBrand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mind on 6/19/2017.
 */

public class AllBrand extends AppCompatActivity implements LoadAllBrand.ViewData {
    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.list)
    ListView list;
    LoadAllBrand loadData;
    ResultAllBrand rab = new ResultAllBrand();

    Handler handler = new Handler();
    ArrayList<HashMap<String,String>> data = new ArrayList<>();
    BaseAdapter adapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_brand);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("All Brand");

        loadData = new LoadAllBrand(this);
        loadDataBrand();

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                ((Filterable) AllBrand.this.list.getAdapter()).getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void loadDataBrand() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                loadData.loadDataJson(AllBrand.this);
            }
        });
    }

    @Override
    public void getData(ResponseDataBrand model) {
        try {
            JSONArray array = new JSONArray(model.getData());
            for (int a = 0; a < array.length(); a++){
                JSONObject object = array.getJSONObject(a);
                HashMap<String,String> map = new HashMap<>();
                map.put(rab.getSlug(), object.getString(rab.getSlug()));
                data.add(map);
            }
            showData(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showData(final ArrayList<HashMap<String, String>> data) {
        adapter = new BrandAdapter(this, data);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent a =  new Intent(AllBrand.this, ListBrand.class);
                a.putExtra("slug", ((TextView) view.findViewById(R.id.item)).getText().toString());
                startActivity(a);
            }
        });
    }
}
