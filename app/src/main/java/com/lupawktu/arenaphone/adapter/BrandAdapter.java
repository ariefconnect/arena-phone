package com.lupawktu.arenaphone.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.lupawktu.arenaphone.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mind on 6/20/2017.
 */
public class BrandAdapter extends BaseAdapter implements Filterable{
    Activity activity;
    ArrayList<HashMap<String,String>> data;
    ArrayList<HashMap<String,String>> filterData;
    private static LayoutInflater inflater;
    private ValueFilter valueFilter;

    public BrandAdapter(Activity activity, ArrayList<HashMap<String,String>> data) {
        this.activity = activity;
        this.data = data;
        this.filterData = data;
        inflater = (LayoutInflater)  activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.item_list, null);
        TextView item = (TextView) view.findViewById(R.id.item);
        item.setAllCaps(true);
        item.setText(data.get(i).get("slug"));
        return view;
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results = new FilterResults();
            if(charSequence !=null && charSequence.length()>0) {
                ArrayList<HashMap<String, String>> filterList = new ArrayList<>();

                for (int i = 0; i < filterData.size(); i++) {
                    if (filterData.get(i).get("slug").toUpperCase()
                            .contains(charSequence.toString().toUpperCase())) {
                        HashMap<String,String> filter = new HashMap<>();
                        filter.put("slug",filterData.get(i).get("slug"));
                        filterList.add(filter);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = filterData.size();
                results.values = filterData;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            data = (ArrayList<HashMap<String,String>>) filterResults.values;
            notifyDataSetChanged();
        }
    }
}
