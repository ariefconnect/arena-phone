package com.lupawktu.arenaphone.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.lupawktu.arenaphone.ListBrand;
import com.lupawktu.arenaphone.R;
import com.lupawktu.arenaphone.model.DataList;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mind on 7/8/2017.
 */
public class ListBrandAdapter extends BaseAdapter implements Filterable {
    Activity activity;
    ArrayList<DataList> data;
    ArrayList<DataList> filterData;
    private static LayoutInflater inflater;
    private ValueFilter valueFilter;

    DataList dl;

    public ListBrandAdapter(Activity activity, ArrayList<DataList> data) {
        this.activity = activity;
        this.data = data;
        this.filterData = data;
        inflater = (LayoutInflater)  activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.item_list_brand, null);
        TextView item = (TextView) view.findViewById(R.id.item);
        TextView slug = (TextView) view.findViewById(R.id.slug);
        item.setAllCaps(true);
//        item.setText(data.get(i).get("title"));
        item.setText(data.get(i).getTitle());
        slug.setText(data.get(i).getSlug());
        return view;
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results = new FilterResults();
            if(charSequence !=null && charSequence.length()>0) {
                ArrayList<DataList> filterList = new ArrayList<>();

                for (int i = 0; i < filterData.size(); i++) {
                    if (filterData.get(i).getTitle().toUpperCase()
                            .contains(charSequence.toString().toUpperCase())) {
                        dl = new DataList();
                        dl.setSlug(filterData.get(i).getSlug());
                        dl.setTitle(filterData.get(i).getTitle());
                        filterList.add(dl);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = filterData.size();
                results.values = filterData;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            data = (ArrayList<DataList>) filterResults.values;
            notifyDataSetChanged();
        }
    }
}
