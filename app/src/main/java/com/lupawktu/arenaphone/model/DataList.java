package com.lupawktu.arenaphone.model;

/**
 * Created by Mind on 7/8/2017.
 */

public class DataList {
    private String title = "title";
    private String slug = "slug";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
