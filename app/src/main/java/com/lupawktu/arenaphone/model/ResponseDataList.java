package com.lupawktu.arenaphone.model;

/**
 * Created by Mind on 7/8/2017.
 */

public class ResponseDataList {
    private String data;
    private int code;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
