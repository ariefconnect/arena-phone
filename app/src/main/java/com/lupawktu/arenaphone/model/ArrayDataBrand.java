package com.lupawktu.arenaphone.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mind on 7/8/2017.
 */

public class ArrayDataBrand {
    private ArrayList<DataList> dataLists;

    public ArrayList<DataList> getDataLists() {
        return dataLists;
    }

    public void setDataLists(ArrayList<DataList> dataLists) {
        this.dataLists = dataLists;
    }
}
