package com.lupawktu.arenaphone;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.lupawktu.arenaphone.Result.ResultAllBrand;
import com.lupawktu.arenaphone.adapter.ListBrandAdapter;
import com.lupawktu.arenaphone.model.ArrayDataBrand;
import com.lupawktu.arenaphone.model.ResponseDataList;
import com.lupawktu.arenaphone.presenter.LoadBrandList;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mind on 6/25/2017.
 */
public class ListBrand extends AppCompatActivity implements LoadBrandList.ViewBrand {
    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.loading)
    ProgressBar loading;
    ResultAllBrand rab = new ResultAllBrand();
    Handler handler = new Handler();
    LoadBrandList lbl = new LoadBrandList(this);
    BaseAdapter adapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_brand);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle(getIntent().getStringExtra(rab.getSlug()));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        RunBackground();

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                ((Filterable) ListBrand.this.list.getAdapter()).getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void RunBackground() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                lbl.getDataListBrand(getIntent().getStringExtra(rab.getSlug()));
            }
        }, 3000);
    }

    /**
     * success get data
     * @param model
     */
    @Override
    public void getData(ResponseDataList model) {
        loading.setVisibility(View.GONE);
        try {
            JSONObject jsonObject = new JSONObject(model.getData());
            if ( jsonObject.getString("status").equalsIgnoreCase("sukses") ) {
//                Log.e("data", jsonObject.getString("data"));
                lbl.SavaDataToArray(jsonObject.getString("data"));
            } else {
                dialogError("Error get data. Please try again");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * show error get data
     * @param pesan
     */
    private void dialogError(String pesan) {
        new AlertDialog.Builder(this)
                .setMessage(pesan)
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setPositiveButton("Reload", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RunBackground();
                        loading.setVisibility(View.VISIBLE);
                    }
                })
                .create().show();
    }

    /**
     * fail get data
     * @param model
     */
    @Override
    public void failGetData(ResponseDataList model) {
        loading.setVisibility(View.GONE);
        dialogError(model.getData());
    }

    /**
     * show Data from array
     * @param model
     */
    @Override
    public void showData(final ArrayDataBrand model) {
        adapter = new ListBrandAdapter(this, model.getDataLists());
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent a = new Intent(ListBrand.this, DetailSpec.class);
                a.putExtra("slug", ((TextView) view.findViewById(R.id.slug)).getText().toString());
                a.putExtra("title", ((TextView) view.findViewById(R.id.item)).getText().toString());
                startActivity(a);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
