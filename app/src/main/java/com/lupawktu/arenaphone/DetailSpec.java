package com.lupawktu.arenaphone;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.lupawktu.arenaphone.model.ResponseDataList;
import com.lupawktu.arenaphone.presenter.LoadDataDetail;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mind on 7/9/2017.
 */
public class DetailSpec extends AppCompatActivity implements LoadDataDetail.View {
    @BindView(R.id.layout) ScrollView layout;
    @BindView(R.id.loading) ProgressBar loading;
    @BindView(R.id.tech) TextView tech;
    @BindView(R.id.dua_g) TextView duag;
    @BindView(R.id.tiga_g) TextView tigag;
    @BindView(R.id.empat_g) TextView empatg;
    @BindView(R.id.speed) TextView speed;
    @BindView(R.id.gprs) TextView gprs;
    @BindView(R.id.edge) TextView edge;
    @BindView(R.id.announced) TextView announced;
    @BindView(R.id.status) TextView status;
    @BindView(R.id.dimensions) TextView dimension;
    @BindView(R.id.weight) TextView weight;
    @BindView(R.id.sim) TextView sim;
    @BindView(R.id.type) TextView type;
    @BindView(R.id.size) TextView size;
    @BindView(R.id.resolution) TextView resolution;
    @BindView(R.id.multitouch) TextView multitouch;
    @BindView(R.id.protection) TextView protection;
    @BindView(R.id.os) TextView os;
    @BindView(R.id.chipset) TextView chipset;
    @BindView(R.id.cpu) TextView cpu;
    @BindView(R.id.gpu) TextView gpu;
    @BindView(R.id.card_slot) TextView card_slot;
    @BindView(R.id.internal) TextView internal;
    @BindView(R.id.primary) TextView primary;
    @BindView(R.id.features) TextView features;
    @BindView(R.id.video) TextView video;
    @BindView(R.id.secondary) TextView secondary;
    @BindView(R.id.alert) TextView alert;
    @BindView(R.id.loudspeaker) TextView loudspeaker;
    @BindView(R.id.mm_jack) TextView mmjack;
    @BindView(R.id.wlan) TextView wlan;
    @BindView(R.id.bluetooth) TextView bluetooth;
    @BindView(R.id.gps) TextView gps;
    @BindView(R.id.infrared) TextView infrared;
    @BindView(R.id.radio) TextView radio;
    @BindView(R.id.usb) TextView usb;
    @BindView(R.id.sensor) TextView sensor;
    @BindView(R.id.messaging) TextView messaging;
    @BindView(R.id.browser) TextView browser;
    @BindView(R.id.java) TextView java;
    @BindView(R.id.battype) TextView battype;
    @BindView(R.id.standby) TextView standby;
    @BindView(R.id.talk_time) TextView talktime;
    @BindView(R.id.music_play) TextView musicplay;
    @BindView(R.id.color) TextView color;
    @BindView(R.id.price) TextView price;

    @BindView(R.id.imgView)
    ImageView imageView;

    Handler handler = new Handler();
    LoadDataDetail ldd = new LoadDataDetail(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_spec_activity);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle(getIntent().getStringExtra("title"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        layout.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
        getData();
    }

    private void getData() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ldd.getDataSpecDetail(getIntent().getStringExtra("slug"));
            }
        }, 3000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void getDataSuccess(ResponseDataList model) {
        loading.setVisibility(View.GONE);
        layout.setVisibility(View.VISIBLE);
        try {
            JSONObject object = new JSONObject(model.getData());
            if ( object.getString("status").equalsIgnoreCase("sukses") ) {
                Glide.with(this)
                        .load(object.getString("img"))
                        .into(imageView);

                JSONObject obj1 = object.getJSONObject("data");
                JSONObject obj2 = obj1.getJSONObject("network");
                JSONObject obj3 = obj1.getJSONObject("launch");
                JSONObject obj4 = obj1.getJSONObject("body");
                JSONObject obj5 = obj1.getJSONObject("display");
                JSONObject obj6 = obj1.getJSONObject("platform");
                JSONObject obj7 = obj1.getJSONObject("memory");
                JSONObject obj8 = obj1.getJSONObject("sound");
                JSONObject obj9 = obj1.getJSONObject("camera");
                JSONObject obj10 = obj1.getJSONObject("comms");
                JSONObject obj11 = obj1.getJSONObject("features");
                JSONObject obj12 = obj1.getJSONObject("battery");
                JSONObject obj13 = obj1.getJSONObject("misc");

                showDataNetwork(obj2);
                showDataLaunch(obj3);
                showDataBody(obj4);
                showDataDisplay(obj5);
                showDataPlatform(obj6);
                showDataMemory(obj7);
                showDataSound(obj8);
                showDataCamera(obj9);
                showDataComms(obj10);
                showDataFeatures(obj11);
                showDataBattery(obj12);
                showDataMisc(obj13);

            } else {
                dialogError("Error get data. Please try again");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * show data misc
     * @param obj13
     */
    private void showDataMisc(JSONObject obj13) {
        try {
            color.setText(obj13.getString("colors"));
            price.setText(obj13.getString("price"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * show data battery
     * @param obj12
     */
    private void showDataBattery(JSONObject obj12) {
        try {
            battype.setText(obj12.getString("_empty_"));
            standby.setText(obj12.getString("stand_by"));
            talktime.setText(obj12.getString("talk_time"));
            musicplay.setText(obj12.getString("music_play"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * show data features
     * @param obj11
     */
    private void showDataFeatures(JSONObject obj11) {
        try {
            sensor.setText(obj11.getString("sensors"));
            messaging.setText(obj11.getString("messaging"));
            browser.setText(obj11.getString("browser"));
            String javad = obj11.getString("java").replace("<br>","\n");
            java.setText(javad);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * show data comms
     * @param obj10
     */
    private void showDataComms(JSONObject obj10) {
        try {
            wlan.setText(obj10.getString("wlan"));
            bluetooth.setText(obj10.getString("bluetooth"));
            gps.setText(obj10.getString("gps"));
            infrared.setText(obj10.getString("infrared_port"));
            radio.setText(obj10.getString("radio"));
            usb.setText(obj10.getString("usb"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * show data camera
     * @param obj9
     */
    private void showDataCamera(JSONObject obj9) {
        try {
            primary.setText(obj9.getString("primary"));
            features.setText(obj9.getString("features"));
            video.setText(obj9.getString("video"));
            secondary.setText(obj9.getString("secondary"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * show data sound
     * @param obj8
     */
    private void showDataSound(JSONObject obj8) {
        try {
            alert.setText(obj8.getString("alert_types"));
            loudspeaker.setText(obj8.getString("loudspeaker_"));
            mmjack.setText(obj8.getString("35mm_jack_"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * show data memory
     * @param obj7
     */
    private void showDataMemory(JSONObject obj7) {
        try {
            card_slot.setText(obj7.getString("card_slot"));
            internal.setText(obj7.getString("internal"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * show data platform
     * @param obj6
     */
    private void showDataPlatform(JSONObject obj6) {
        try {
            os.setText(obj6.getString("os"));
            chipset.setText(obj6.getString("chipset"));
            cpu.setText(obj6.getString("cpu"));
            gpu.setText(obj6.getString("gpu"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * show data display
     * @param obj5
     */
    private void showDataDisplay(JSONObject obj5) {
        try {
            type.setText(obj5.getString("type"));
            size.setText(obj5.getString("size"));
            resolution.setText(obj5.getString("resolution"));
            multitouch.setText(obj5.getString("multitouch"));
            protection.setText(obj5.getString("protection"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * show data Body
     * @param obj4
     */
    private void showDataBody(JSONObject obj4) {
        try {
            dimension.setText(obj4.getString("dimensions"));
            weight.setText(obj4.getString("weight"));
            sim.setText(obj4.getString("sim"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * show data launch
     * @param obj3
     */
    private void showDataLaunch(JSONObject obj3) {
        try {
            announced.setText(obj3.getString("announced"));
            status.setText(obj3.getString("status"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * show data network
     * @param obj2
     */
    private void showDataNetwork(JSONObject obj2) {
        try {
            tech.setText(obj2.getString("technology"));
            duag.setText(obj2.getString("2g_bands").replace("<br>","\n"));
            tigag.setText(obj2.getString("3g_bands").replace("<br>","\n"));
            empatg.setText(obj2.getString("4g_bands").replace("<br>","\n"));
            speed.setText(obj2.getString("speed"));
            gprs.setText(obj2.getString("gprs"));
            edge.setText(obj2.getString("edge"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void dialogError(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setPositiveButton("Refresh", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getData();
                        loading.setVisibility(View.VISIBLE);
                        layout.setVisibility(View.GONE);
                    }
                })
                .create().show();
    }

    @Override
    public void getDataFailure(ResponseDataList model) {
        loading.setVisibility(View.GONE);
        layout.setVisibility(View.GONE);
    }
}
