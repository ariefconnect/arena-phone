package com.lupawktu.arenaphone;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.all)
    RelativeLayout allBrand;
    @BindView(R.id.search)
    RelativeLayout search;
    @BindView(R.id.favorite)
    RelativeLayout favorite;
    @BindView(R.id.aplikasi)
    RelativeLayout aplikasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        allBrand.setOnClickListener(this);
        search.setOnClickListener(this);
        favorite.setOnClickListener(this);
        aplikasi.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.all :
                Log.i("Intent", "all brand");
                Intent a = new Intent(this, AllBrand.class);
                startActivity(a);
                break;
            case R.id.search :
                Log.i("Intent", "search");
                break;
            case R.id.favorite :
                Log.i("Intent", "favorite");
                break;
            case R.id.aplikasi :
                Log.i("Intent", "aplikasi");
                break;
        }
    }
}
